package jala.university.api.model.repository;

import jala.university.api.model.entity.Product;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class ProductRepository {
    private final Set<Product> products;

    public ProductRepository() {
        this.products = new HashSet<>();
    }

    public boolean create(Product product) {
        return products.add(product);
    }

    public Set<Product> readAll() {
        return products;
    }

    public Product readByName(String name) {
        return products.stream()
            .filter(object -> object.getName().equals(name))
            .findFirst()
            .orElse(null);
    }

    public Set<Product> readByPrice(double initialValue, double finalValue) {
        return products.stream()
            .filter(object -> object.getPrice() >= initialValue && object.getPrice() <= finalValue)
            .collect(Collectors.toSet());
    }

    public Set<Product> readByQuantity(int initialValue, int finalValue) {
        return products.stream()
            .filter(object -> object.getQuantity() >= initialValue && object.getQuantity() <= finalValue)
            .collect(Collectors.toSet());
    }

    public boolean update(Product product) {
        return products.stream()
            .filter(object -> product.getId().equals(object.getId()))
            .findFirst()
            .map(object -> {
                products.remove(object);
                products.add(product);
                return true;
            })
            .orElse(false);
    }

    public boolean deleteAll() {
        products.clear();
        return true;
    }

    public boolean delete(Product product) {
        return products.stream()
            .filter(object -> product.getId().equals(object.getId()))
            .findFirst()
            .map(object -> {
                products.remove(object);
                return true;
            })
            .orElse(false);
    }
}
