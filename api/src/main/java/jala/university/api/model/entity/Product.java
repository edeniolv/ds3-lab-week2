package jala.university.api.model.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class Product {
    private UUID id;
    private String name;
    private String description;
    private double price;
    private long quantity;

    public Product() {
        this.id = UUID.randomUUID();
    }
}
