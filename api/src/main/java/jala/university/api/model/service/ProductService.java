package jala.university.api.model.service;

import jala.university.api.model.entity.Product;
import jala.university.api.model.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Set;

@Service
public class ProductService {
    private final ProductRepository repository;

    @Autowired
    public ProductService(ProductRepository repository) {
        this.repository = repository;
    }

    public boolean add(Product product) {
        return repository.create(product);
    }

    public Set<Product> findAll() {
        if (isEmpty()) {
            return Collections.emptySet();
        }

        return repository.readAll();
    }

    public Product findByName(String name) {
        if (name.isEmpty()) {
            return null;
        }

        return repository.readByName(name);
    }

    public Set<Product> findByPrice(double initialValue, double finalValue) {
        if (isBiggerThan(initialValue)) {
            return Collections.emptySet();
        }

        if (isBiggerThan(finalValue)) {
            return Collections.emptySet();
        }

        return repository.readByPrice(initialValue, finalValue);
    }

    public Set<Product> findByQuantity(int initialValue, int finalValue) {
        if (isBiggerThan(initialValue)) {
            return Collections.emptySet();
        }

        if (isBiggerThan(finalValue)) {
            return Collections.emptySet();
        }
        
        return repository.readByQuantity(initialValue, finalValue);
    }

    public boolean update(Product product) {
        return repository.update(product);
    }

    public boolean removeAll() {
        if (isEmpty()) {
            return false;
        }

        return repository.deleteAll();
    }

    public boolean remove(Product product) {
        return repository.delete(product);
    }

    private boolean isEmpty() {
        return repository.readAll().isEmpty();
    }

    private <T> boolean isBiggerThan(T value) {
        if (value instanceof Integer number) {
            return number <= 0;
        }
        
        if (value instanceof Double number) {
            return number <= 0;
        }
        
        return true;
    }
}
