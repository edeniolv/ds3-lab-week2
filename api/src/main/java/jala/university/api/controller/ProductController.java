package jala.university.api.controller;

import jala.university.api.model.entity.Product;
import jala.university.api.model.service.ProductService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.logging.Level;

@RestController
@RequestMapping("/Products")
@Log
public class ProductController {
    private final ProductService service;

    @Autowired
    public ProductController(ProductService service) {
        this.service = service;
    }

    @GetMapping("/FindBy")
    public ResponseEntity<Product> findProductByName(@RequestParam String name) {
        Product product = service.findByName(name);

        if (product == null) {
            log.log(Level.INFO, "Product {0} not found", name);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        log.log(Level.INFO, "Product {0} found", product.getName());
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @GetMapping("/FindByPrice")
    public ResponseEntity<Set<Product>> findProductByPrice(
        @RequestParam double initialValue, @RequestParam double finalValue
    ) {
        Set<Product> products = service.findByPrice(initialValue, finalValue);
        log.log(Level.INFO, "Find product by price interval");
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/FindByQuantity")
    public ResponseEntity<Set<Product>> findProductByQuantity(
        @RequestParam int initialValue, @RequestParam int finalValue
    ) {
        Set<Product> products = service.findByQuantity(initialValue, finalValue);
        log.log(Level.INFO, "Find product by quantity interval");
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Set<Product>> findAllProducts() {
        Set<Product> products = service.findAll();
        log.log(Level.INFO, "Find all products");
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @PostMapping("/Add")
    public ResponseEntity<String> addProduct(@RequestBody Product product) {
        if (service.add(product)) {
            log.log(Level.INFO, "Product {0} added", product.getName());
            return new ResponseEntity<>(HttpStatus.CREATED);
        }

        return null;
    }

    @PostMapping("/Update")
    public HttpStatus updateProduct(@RequestBody Product product) {
        if (service.update(product)) {
            log.log(Level.INFO, "Product {0} updated", product.getName());
            return HttpStatus.CREATED;
        }
        return null;
    }

    @DeleteMapping("/Remove")
    public HttpStatus deleteProduct(@RequestBody Product product) {
        if (service.remove(product)) {
            log.log(Level.INFO, "Product {0} removed", product.getName());
            return HttpStatus.OK;
        }
        return null;
    }

    @DeleteMapping("/RemoveAll")
    public HttpStatus deleteAllProducts() {
        if (service.removeAll()) {
            log.log(Level.INFO, "Remove all products");
            return HttpStatus.OK;
        }
        return null;
    }
}
