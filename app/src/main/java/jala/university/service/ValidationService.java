package jala.university.service;

public class ValidationService {

    @SafeVarargs
    public final <T> boolean validate(T... values) {
        for (T value : values) {
            if (isNotValid(value)) {
                return false;
            }
        }

        return true;
    }


    private <T> boolean isNotValid(T object) {
        if (object instanceof String value) {
            return value.isEmpty();
        }

        if (object instanceof Double value) {
            return value <= 0;
        }

        if (object instanceof Integer value) {
            return value <= 0;
        }

        return false;
    }
}
