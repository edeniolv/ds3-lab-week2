package jala.university.service;

import jala.university.utils.HttpEndpoint;
import jala.university.utils.HttpMethod;
import jala.university.utils.Request;
import org.json.JSONObject;

import java.net.http.HttpResponse;

public class RequestService {
    private final JSONObject json;
    private final Request request;
    private HttpResponse<String> response;

    public RequestService() {
        json = new JSONObject();
        request = new Request(json);
    }

    public <T> void buildProperty(String key, T value) {
        json.put(key, value);
    }

    public void buildRequest(HttpMethod method, HttpEndpoint endpoint) {
        request.setMethod(method);
        request.setHeader();
        request.setEndpoint(endpoint);
        request.build();
    }

    public void sentRequest() {
        response = request.sent();
    }

    public HttpResponse<String> getResponse() {
        return response;
    }
}
