package jala.university.utils;

public enum HttpMethod {
    GET,
    POST,
    PUT,
    DELETE
}
