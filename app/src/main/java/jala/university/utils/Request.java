package jala.university.utils;

import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Request {
    private final HttpClient httpClient;
    private final HttpRequest.Builder httpRequestbuilder;
    private HttpRequest httpRequest;
    private final JSONObject json;

    public Request(JSONObject json) {
        this.httpClient = HttpClient.newHttpClient();
        this.httpRequestbuilder = HttpRequest.newBuilder();
        this.json = json;
    }

    public void setMethod(HttpMethod method) {
        switch (method) {
            case GET -> httpRequestbuilder.GET();
            case POST -> httpRequestbuilder.POST(
                HttpRequest.BodyPublishers.ofByteArray(json.toString().getBytes())
            );
            case PUT -> httpRequestbuilder.PUT(
                HttpRequest.BodyPublishers.ofByteArray(json.toString().getBytes())
            );
            case DELETE -> httpRequestbuilder.DELETE();
            default -> System.out.println("\n* ONLY OPTION VALID\"");
        }
    }

    public void setHeader() {
        httpRequestbuilder.setHeader(
            "Content-Type", "application/json"
        );
    }

    public void setEndpoint(HttpEndpoint url) {
        httpRequestbuilder.uri(URI.create(url.getValue()));
    }

    public void build() {
        httpRequest = httpRequestbuilder.build();
    }

    public HttpResponse<String> sent() {
        HttpResponse<String> response = null;

        try {
            response = httpClient.send(
                httpRequest, HttpResponse.BodyHandlers.ofString()
            );
        } catch (InterruptedException | IOException exception) {
            Thread.currentThread().interrupt();
        }

        return response;
    }
}
