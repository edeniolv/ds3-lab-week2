package jala.university.utils;

public enum HttpEndpoint {
    ADD("http://localhost:8080/Products/Add"),
    FIND_BY_NAME("http://localhost:8080/Products/Name"),
    FIND_BY_PRICE("http://localhost:8080/Products/Price"),
    FIND_BY_QUANTITY("http://localhost:8080/Products/Quantity"),
    FIND_ALL("http://localhost:8080/Products"),
    UPDATE("http://localhost:8080/Products/Update"),
    REMOVE("http://localhost:8080/Products/Remove"),
    REMOVE_ALL("http://localhost:8080/Products/RemoveAll");

    private final String value;

    HttpEndpoint(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
