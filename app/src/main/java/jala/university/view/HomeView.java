package jala.university.view;

import java.util.Scanner;

public class HomeView {
    public HomeView() {
        build();
    }

    private void build() {
        Scanner input = new Scanner(System.in);
        boolean running = true;

        while (running) {
            System.out.print("""
            
                # STOCK MANAGEMENT
                #
                # [1] CREATE
                # [2] READ
                # [3] UPDATE
                # [4] DELETE
                # [0] EXIT
                #
                # ->\s"""
            );

            String option = input.nextLine();

            switch (option) {
                case "1" -> new CreateView(input);
                case "2" -> new ReadView();
                case "3" -> new UpdateView();
                case "4" -> new DeleteView();
                case "0" -> running = false;
                default -> System.out.println("\n* ONLY OPTION VALID\"");
            }
        }
    }
}
