package jala.university.view;

import jala.university.service.RequestService;
import jala.university.service.ValidationService;
import jala.university.utils.HttpEndpoint;
import jala.university.utils.HttpMethod;

import java.util.Scanner;

public class CreateView {
    private final Scanner input;
    private final ValidationService validationService;
    private final RequestService requestService;
    private boolean running;
    private String name;
    private String description;
    private double price;
    private int quantity;

    public CreateView(Scanner input) {
        this.running = true;
        this.input = input;
        this.validationService = new ValidationService();
        this.requestService = new RequestService();

        build();
    }

    private void build() {
        try {
            while (running) {
                System.out.println("\n# CREATE PRODUCT\n#");

                buildFields();
                validateFields();
                buildJson();
                buildRequest();

                if (requestService.getResponse().statusCode() == 201) {
                    System.out.printf("\n" + "* PRODUCT '%S' ADDED", name);
                    running = false;
                }
            }
        } catch (NumberFormatException exception) {
            System.out.println("\n* NUMBER IS INVALID");
            System.out.println("* RETURNING TO HOME");
        }
    }

    private void buildFields() {
        System.out.print("# NAME: ");
        name = input.nextLine().toUpperCase();

        System.out.print("# DESCRIPTION: ");
        description = input.nextLine();

        System.out.print("# PRICE: ");
        price = Double.parseDouble(
            input.nextLine().replace(",", ".")
        );

        System.out.print("# QUANTITY: ");
        quantity = Integer.parseInt(input.nextLine());
    }

    private void validateFields() {
        boolean isValid = validationService.validate(
            name, description, price, quantity
        );

        if (!isValid) {
            System.out.println("\n* FIELD ARE EMPTY OR IS INVALID");
            System.out.println("* RETURNING TO HOME");
            running = false;
        }
    }

    private void buildJson() {
        toJson("name", name);
        toJson("description", description);
        toJson("price", price);
        toJson("quantity", quantity);
    }

    private void buildRequest() {
        requestService.buildRequest(HttpMethod.POST, HttpEndpoint.ADD);
        requestService.sentRequest();
    }

    private <T> void toJson(String key, T value) {
        requestService.buildProperty(key, value);
    }
}
